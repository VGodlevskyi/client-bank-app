package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.repositories.AccountRepository;
import ClientBankSpringApp.repositories.CustomerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class CustomerServiceTest {
    private CustomerService customerService;
    private CustomerRepository customerRepository;
    private AccountService accountService;
    private AccountRepository accountRepository;

    @Autowired
    public CustomerServiceTest(CustomerService customerService, CustomerRepository customerRepository, AccountService accountService, AccountRepository accountRepository) {
        this.customerService = customerService;
        this.customerRepository = customerRepository;
        this.accountService = accountService;
        this.accountRepository = accountRepository;
    }

    @Test
    public void getAllCustomersTest() {
        Assertions.assertNotNull(customerService.getAllCustomers(1, 1));
    }

    @Test
    public void deleteCustomerByIdTest() {
        customerService.deleteById(1L);
        Assertions.assertEquals(customerRepository.findAll().size(), 1);
    }

    @Test
    public void getCustomerByIdTest() {
        Assertions.assertNotNull(customerService.getCustomer(2L));
    }

    @Test
    public void addCustomerTest() {
        customerService.create(new Customer());
        Assertions.assertEquals(customerRepository.findAll().size(), 2);
    }
}