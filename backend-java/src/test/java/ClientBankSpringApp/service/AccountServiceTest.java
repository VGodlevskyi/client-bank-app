package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.enumerations.Currency;
import ClientBankSpringApp.repositories.AccountRepository;
import ClientBankSpringApp.repositories.CustomerRepository;
import ClientBankSpringApp.repositories.EmployerRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class AccountServiceTest {
    private EmployerService employerService;
    private EmployerRepository employerRepository;

    private CustomerService customerService;
    private CustomerRepository customerRepository;

    private AccountService accountService;
    private AccountRepository accountRepository;

    @Autowired
    public AccountServiceTest(EmployerService employerService, EmployerRepository employerRepository, CustomerService customerService, CustomerRepository customerRepository, AccountService accountService, AccountRepository accountRepository) {
        this.employerService = employerService;
        this.employerRepository = employerRepository;
        this.customerService = customerService;
        this.customerRepository = customerRepository;
        this.accountService = accountService;
        this.accountRepository = accountRepository;
    }

    @Test
    public void getAllAccountsTest() {
        Assertions.assertNotNull(accountService.getAll());
    }

    @Test
    public void deleteAccountByIdTest() {
        accountService.deleteById(1L);
        Assertions.assertEquals(accountRepository.findAll().size(), 2);
    }

    @Test
    public void addAccountTest() {
        Customer customer = new Customer();
        Account account = new Account(Currency.GBP, customer);
        accountService.saveAccount(account);
        Assertions.assertEquals(accountRepository.findAll().size(), 3);
    }

}