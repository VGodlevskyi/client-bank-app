package ClientBankSpringApp.controller;

import ClientBankSpringApp.service.AccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private AccountService accountService;

    @Test
    void putMoney() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/account/replenish");
        this.mockMvc.perform(request).andExpect(status().isMethodNotAllowed());
    }

    @Test
    void withdrawMoney() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/account/withdraw");
        this.mockMvc.perform(request).andExpect(status().isMethodNotAllowed());
    }

    @Test
    void transferMoney() throws Exception {
        RequestBuilder request = MockMvcRequestBuilders.get("/account/transfer");
        this.mockMvc.perform(request).andExpect(status().isMethodNotAllowed());
    }
}