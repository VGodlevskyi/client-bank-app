package ClientBankSpringApp.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@Data
@Entity
@Table(name = "users")
public class DbUser {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;
  private String email;
  private String password;
  private String roles;

  @Transient
  private final String ROLES_DELIMITER = ":";

  public DbUser(String email, String password, String... roles) {
    this.email = email;
    this.password = password;
    setRoles(roles);
  }

  public String[] getRoles() {
    if (this.roles == null || this.roles.isEmpty()) return new String[]{};
    return this.roles.split(ROLES_DELIMITER);
  }

  public void setRoles(String[] roles) {
    this.roles = String.join(ROLES_DELIMITER, roles);
  }

}
