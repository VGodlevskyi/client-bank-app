package ClientBankSpringApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ID")
@Data
@Entity
@Table(name = "employers")
@EqualsAndHashCode(callSuper = true)
public class Employer extends AbstractEntity<Employer> {

     String name;
     String address;

     @JsonIgnore
     @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
     @JoinTable(name = "employers_to_customers",
             joinColumns = @JoinColumn(name = "employer_id"),
             inverseJoinColumns = @JoinColumn(name = "customer_id"))
     private List<Customer> customers = new ArrayList<>();

}
