package ClientBankSpringApp.entity;

import ClientBankSpringApp.enumerations.Currency;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.UUID;

@SuppressWarnings("ID")
@Entity
@Data
@ToString
@RequiredArgsConstructor
@Table(name = "accounts")
@EqualsAndHashCode(callSuper = false)
public class Account extends AbstractEntity<Account>{

    @Column(unique = true)
    private String number;

    private Currency currency;

    private Double balance;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
    @JoinTable(name = "accounts_to_customer",
            joinColumns = @JoinColumn(name = "account_id"),
            inverseJoinColumns = @JoinColumn(name = "customer_id"))
    private Customer customer;


    public Account(Currency currency, Customer customer) {
        this.number = UUID.randomUUID().toString();
        this.currency = currency;
        this.balance = 0d;
        this.customer = customer;
    }
}
