package ClientBankSpringApp.repositories;

import ClientBankSpringApp.entity.DbUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DbUserRepo extends JpaRepository<DbUser, Long> {
  Optional<DbUser> findByEmail(String email);
}
