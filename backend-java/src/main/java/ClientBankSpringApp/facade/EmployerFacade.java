package ClientBankSpringApp.facade;

import ClientBankSpringApp.dto.EmployerRequest;
import ClientBankSpringApp.dto.EmployerResponse;
import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.service.EmployerService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class EmployerFacade implements Facade<Employer, EmployerResponse, EmployerRequest> {

    private final ModelMapper mapper = new ModelMapper();
    @Autowired
    EmployerService employerService;

    @Override
    public EmployerResponse toDTOResponse(EmployerRequest employerRequest) {
        return mapper.map(employerRequest, EmployerResponse.class);
    }

    @Override
    public EmployerRequest toDTORequest(EmployerResponse employerResponse) {
        return mapper.map(employerResponse, EmployerRequest.class);
    }

    @Override
    public EmployerResponse fromEntity(Employer entity) {
        return mapper.map(entity, EmployerResponse.class);
    }

    @Override
    public Employer toEntity(EmployerRequest employerRequest) {
        return mapper.map(employerRequest, Employer.class);
    }



    public List<EmployerResponse> getAllEmployers() {
        return employerService
                .getAll().stream()
                .map(this::fromEntity)
                .collect(Collectors.toList());
    }

    public EmployerResponse getEmployer(Long id) {
        Employer employer = employerService.getById(id);
        return fromEntity(employer);
    }

    public Employer createEmployer(EmployerRequest employerRequest) {
        return employerService.save(this.toEntity(employerRequest));
    }

    public void deleteEmployer(EmployerRequest employerRequest) {
        employerService.delete(this.toEntity(employerRequest).getId());
    }
}
