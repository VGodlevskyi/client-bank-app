package ClientBankSpringApp.facade;

import ClientBankSpringApp.dto.CustomerRequest;
import ClientBankSpringApp.dto.CustomerResponse;
import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.enumerations.Currency;
import ClientBankSpringApp.service.AccountService;
import ClientBankSpringApp.service.CustomerService;
import ClientBankSpringApp.service.EmployerService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CustomerFacade implements Facade<Customer, CustomerResponse, CustomerRequest> {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    CustomerService customerService;
    @Autowired
    AccountService accountService;
    @Autowired
    EmployerService employerService;

    @Override
    public CustomerResponse toDTOResponse(CustomerRequest customerRequest) {
        return mapper.map(customerRequest, CustomerResponse.class);
    }

    @Override
    public CustomerResponse fromEntity(Customer customer) {
        return mapper.map(customer, CustomerResponse.class);
    }

    @Override
    public Customer toEntity(CustomerRequest customerRequest) {
        return mapper.map(customerRequest, Customer.class);
    }

    @Override
    public CustomerRequest toDTORequest(CustomerResponse customerResponse) {
        return mapper.map(customerResponse, CustomerRequest.class);
    }


    public Customer createCustomer(CustomerRequest customerRequest) {
        Customer customer = this.toEntity(customerRequest);
        return customerService.create(customer);
    }

    public List<CustomerResponse> getAllCustomers(int page, int limit) {
        return customerService
                .getAllCustomers(page, limit)
                .stream()
                .map(this::fromEntity)
                .collect(Collectors.toList());
    }

    public Customer updateCustomer(Long id, CustomerRequest customerRequest) {
        Customer customer = customerService.getCustomer(id);
        customer.setName(customerRequest.getName());
        customer.setPhone(customerRequest.getPhone());
        customer.setPassword(customerRequest.getPassword());
        customer.setEmail(customerRequest.getEmail());
        customer.setAge(customerRequest.getAge());
        return customerService.update(id);
    }

    public CustomerResponse getOneCustomer(long id) {
        Customer customer = customerService.getCustomer(id);
        return this.fromEntity(customer);
    }

    public void deleteCustomer(long id) {
        customerService.deleteById(id);
    }

    public void createAccount(long id, String currency) {
        Customer customer = customerService.getCustomer(id);
        Account account = new Account(Currency.getByName(currency.toUpperCase().trim()), customer);
        customer.getAccounts().add(account);
        accountService.saveAccount(account);
    }

    public void setEmployer(long customerID, long employerId) {
        Customer customer = customerService.getCustomer(customerID);
        Employer employer = employerService.getById(employerId);
        customer.getEmployers().add(employer);
        customerService.create(customer);
        employerService.save(employer);
    }
}
