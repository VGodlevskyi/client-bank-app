package ClientBankSpringApp.facade;

import ClientBankSpringApp.dto.AccountRequest;
import ClientBankSpringApp.dto.AccountResponse;
import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.service.AccountService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class AccountFacade implements Facade<Account, AccountResponse, AccountRequest>{
    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    AccountService accountService;

    @Override
    public AccountResponse toDTOResponse(AccountRequest accountRequest) {
         return mapper.map(accountRequest, AccountResponse.class);
    }

    @Override
    public AccountRequest toDTORequest(AccountResponse accountResponse) {
        return mapper.map(accountResponse, AccountRequest.class);
    }

    @Override
    public AccountResponse fromEntity(Account entity) {
        return mapper.map(entity, AccountResponse.class);
    }

    @Override
    public Account toEntity(AccountRequest accountRequest) {
        return mapper.map(accountRequest, Account.class);
    }

    public void deleteAccount(Long id) {
        Optional<Account> account = accountService.get(id);
        if (account.isPresent()) {
            accountService.deleteById(id);
        }
    }

    public void replenish(String number, AccountRequest accountRequest) {
        Optional<Account> account = accountService.getAccount(number);
        if (account.isPresent()) {
            Account a = account.get();
            a.setBalance(a.getBalance() + accountRequest.getBalance());
            accountService.saveAccount(a);
        }
    }

    public void withdrawMoney(String number, AccountRequest accountRequest) {
        Optional<Account> account = accountService.getAccount(number);
        if (account.isPresent() && account.get().getBalance() > accountRequest.getBalance()) {
            Account a = account.get();
            a.setBalance(a.getBalance() - accountRequest.getBalance());
            accountService.saveAccount(a);
        }
    }

    public void transferMoney(String from, String to, double sum) {
        Optional<Account> accountFrom = accountService.getAccount(from);
        Optional<Account> accountTo = accountService.getAccount(to);
        if (accountFrom.isPresent() && accountTo.isPresent() && accountFrom.get().getBalance() > sum) {
            Account af = accountFrom.get();
            Account at = accountTo.get();
            af.setBalance(af.getBalance() - sum);
            at.setBalance(at.getBalance() + sum);
            accountService.saveAccount(af);
            accountService.saveAccount(at);
        }
    }
}
