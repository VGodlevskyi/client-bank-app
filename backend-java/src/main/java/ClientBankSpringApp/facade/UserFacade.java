package ClientBankSpringApp.facade;

import ClientBankSpringApp.dto.CustomerRequest;
import ClientBankSpringApp.dto.CustomerResponse;
import ClientBankSpringApp.dto.UserRequest;
import ClientBankSpringApp.dto.UserResponse;
import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.entity.DbUser;
import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.enumerations.Currency;
import ClientBankSpringApp.service.*;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserFacade implements Facade<DbUser, UserResponse, UserRequest> {

    private final ModelMapper mapper = new ModelMapper();

    @Autowired
    UserService userService;

    @Override
    public UserResponse toDTOResponse(UserRequest userRequest) {
        return mapper.map(userRequest, UserResponse.class);
    }

    @Override
    public UserResponse fromEntity(DbUser user) {
        return mapper.map(user,UserResponse.class);
    }

    @Override
    public DbUser toEntity(UserRequest userRequest) {
        return mapper.map(userRequest, DbUser.class);
    }

    @Override
    public UserRequest toDTORequest(UserResponse userResponse) {
        return mapper.map(userResponse, UserRequest.class);
    }


    public DbUser createUser(UserRequest userRequest) {
        DbUser user = this.toEntity(userRequest);
        return userService.create(user);
    }




    public void deleteUser(long id) {
        userService.deleteById(id);
    }


    public UserResponse getProfile(String name) {
        return fromEntity(userService.getProfile(name));
    }
}
