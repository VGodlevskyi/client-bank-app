package ClientBankSpringApp.exception;

public class CustomerNotFoundException extends BusinessException {
  public final Long id;

  public CustomerNotFoundException(Long id, Throwable x) {
    super(x);
    this.id = id;
  }

  public CustomerNotFoundException(Long id) {
    super();
    this.id = id;
  }
}
