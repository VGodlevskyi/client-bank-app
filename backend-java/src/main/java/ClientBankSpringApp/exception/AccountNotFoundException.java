package ClientBankSpringApp.exception;

public class AccountNotFoundException extends BusinessException {
  public final Long id;

  public AccountNotFoundException(Long id, Throwable x) {
    super(x);
    this.id = id;
  }

  public AccountNotFoundException(Long id) {
    super();
    this.id = id;
  }
}
