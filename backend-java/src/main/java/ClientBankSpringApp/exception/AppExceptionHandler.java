package ClientBankSpringApp.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@Slf4j
@ControllerAdvice
public class AppExceptionHandler {

  @ExceptionHandler({CustomerNotFoundException.class})
  @ResponseBody
  public void handle(CustomerNotFoundException x) {
    log.error("requested customer %d not found", x.id);
  }

  @ExceptionHandler({AccountNotFoundException.class})
  @ResponseBody
  public void handle(AccountNotFoundException x) {
    log.error("requested account %d not found", x.id);
  }

}
