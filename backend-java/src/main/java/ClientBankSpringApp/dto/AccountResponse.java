package ClientBankSpringApp.dto;

import ClientBankSpringApp.enumerations.Currency;
import lombok.Data;

@Data
public class AccountResponse {

  private Long id;
  private String number;
  private Currency currency;
  private Double balance;

}
