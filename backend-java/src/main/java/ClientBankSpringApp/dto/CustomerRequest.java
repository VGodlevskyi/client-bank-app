package ClientBankSpringApp.dto;

import lombok.Data;

import javax.validation.constraints.*;

@Data
public class CustomerRequest {

  @NotBlank(message = "name is required")
  @Size(min = 2, message = "min length - 2 chars")
  private String name;

  @Pattern(regexp = "^([a-zA-Z0-9]+(?:[._+-][a-zA-Z0-9]+)*)@([a-zA-Z0-9]+(?:[.-][a-zA-Z0-9]+)*[.][a-zA-Z]{2,})$",
          message = "email is not valid")
  private String email;

  @Positive(message = "age must be number 18 or bigger")
  @Min(18)
  private Integer age;

  @NotBlank(message = "phone is required")
//  @Pattern(regexp = "^((8|\\+7)[\\- ]?)?(\\(?\\d{3}\\)?[\\- ]?)?[\\d\\- ]{7,10}$",
//          message = "not valid phone number")
  private String phone;

  private String password;

}
