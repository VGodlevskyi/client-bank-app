package ClientBankSpringApp.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class EmployerRequest {

  @NotBlank(message = "name is required")
  @Size(min = 3, message="min length - 3 chars")
  private String name;
  @NotBlank(message = "address is required")
  @Size(min = 3, message="min length - 3 chars")
  private String address;

}
