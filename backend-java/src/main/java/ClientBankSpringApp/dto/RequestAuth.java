package ClientBankSpringApp.dto;

import lombok.Data;

@Data
public class RequestAuth {
    String email;
    String password;

}
