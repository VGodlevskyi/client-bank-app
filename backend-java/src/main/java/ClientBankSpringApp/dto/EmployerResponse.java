package ClientBankSpringApp.dto;

import lombok.Data;

@Data
public class EmployerResponse {
  private Long id;
  private String name;
  private String address;

}
