package ClientBankSpringApp.dto;

import lombok.Data;

import javax.validation.constraints.PositiveOrZero;


@Data
public class AccountRequest {

  private String number;
  @PositiveOrZero(message = "value must be positive number or 0")
  private Double balance;

}
