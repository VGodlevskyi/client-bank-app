package ClientBankSpringApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClientBankSpringApp {
    public static void main(String[] args) {

        SpringApplication.run(ClientBankSpringApp.class, args);
    }
}
