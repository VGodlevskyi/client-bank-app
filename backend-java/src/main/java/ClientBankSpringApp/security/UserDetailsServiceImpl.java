package ClientBankSpringApp.security;

import ClientBankSpringApp.entity.DbUser;
import ClientBankSpringApp.exception.NoDataFoundException;
import ClientBankSpringApp.repositories.DbUserRepo;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final DbUserRepo userRepo;


    public UserDetailsServiceImpl(DbUserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        DbUser user = userRepo.findByEmail(email).orElseThrow(() -> new NoDataFoundException("User not found"));
        return SecurityUser.fromUser(user);
    }
}
