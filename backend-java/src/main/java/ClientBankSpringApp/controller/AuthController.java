package ClientBankSpringApp.controller;

import ClientBankSpringApp.dto.RequestAuth;
import ClientBankSpringApp.dto.UserResponse;
import ClientBankSpringApp.facade.UserFacade;
import ClientBankSpringApp.service.AuthService;
import ClientBankSpringApp.entity.DbUser;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Slf4j
@RestController
@RequestMapping("/api/auth")
public class AuthController  {
    private final AuthService authService;
    private final UserFacade userFacade;

    public AuthController(AuthService authService, UserFacade userFacade) {
        this.authService = authService;
        this.userFacade = userFacade;
    }

    @PostMapping("/login")
    public ResponseEntity<?> authenticate(@RequestBody RequestAuth request) {
        log.info("Authentication - login");
        try {
            return ResponseEntity.ok(authService.authenticate(request.getEmail(), request.getPassword()));

        } catch (AuthenticationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/profile")
    public ResponseEntity<UserResponse> getProfile(Principal principal){
        return ResponseEntity.ok(userFacade.getProfile(principal.getName()));
    }

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody DbUser request) {
        log.info("Authentication - register");
        try {
            return ResponseEntity.ok(authService.register(request));
        } catch (AuthenticationException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.FORBIDDEN);
        }
    }
}
