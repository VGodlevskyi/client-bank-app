package ClientBankSpringApp.controller;

import ClientBankSpringApp.dto.CustomerResponse;
import ClientBankSpringApp.dto.EmployerRequest;
import ClientBankSpringApp.dto.EmployerResponse;
import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.facade.EmployerFacade;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("employers")
public class EmployerController {

    private final EmployerFacade employerFacade;

    @GetMapping("{id}")
    public EmployerResponse getEmployer(@RequestParam("id") long id) {
        log.info("get employer with id %d", id);
        return employerFacade.getEmployer(id);
    }

    @GetMapping("")
    public List<EmployerResponse> getAllEmployers() {
        log.info("get all employers");
        return employerFacade.getAllEmployers();
    }

    @PostMapping("create")
    public Employer createEmployer(@Validated @RequestBody EmployerRequest employerRequest) {
        log.info("create employer");
        return employerFacade.createEmployer(employerRequest);
    }

    @DeleteMapping("delete")
    public void deleteEmployer(@RequestParam Long id, @RequestBody EmployerRequest employerRequest) {
        log.info("delete employer with id %d", id);
        employerFacade.deleteEmployer(employerRequest);
    }
}
