package ClientBankSpringApp.controller;

import ClientBankSpringApp.dto.AccountRequest;
import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.exception.CustomerNotFoundException;
import ClientBankSpringApp.facade.AccountFacade;
import ClientBankSpringApp.service.AccountService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("account")
public class AccountController {
    private final AccountFacade accountFacade;
    private final String WS_PATH = "/account/changes";

    @Autowired
    SimpMessagingTemplate template;


    @PutMapping("{replenish}")
    public void putMoney(@RequestParam("accountNumber") String number, @Validated @RequestBody AccountRequest accountRequest) {
        log.info("replenish account %s", accountRequest.getNumber());
        accountFacade.replenish(number, accountRequest);
        template.convertAndSend(WS_PATH, accountRequest.getBalance());

    }

    @PutMapping({"withdraw"})
    public void withdrawMoney(@RequestParam("accountNumber") String number, @Validated @RequestBody AccountRequest accountRequest) {
        log.info("withdraw account %s", accountRequest.getNumber());
        accountFacade.withdrawMoney(number, accountRequest);
        template.convertAndSend(WS_PATH, accountRequest.getBalance());
    }

    @PutMapping({"transfer"})
    public void transferMoney(@RequestParam("from") String from, @RequestParam("to") String to, @RequestParam("sum") double sum) {
        log.info("transfer from account %s to acccount %s", from, to);
        accountFacade.transferMoney(from, to, sum);
        template.convertAndSend(WS_PATH, sum);
    }


}
