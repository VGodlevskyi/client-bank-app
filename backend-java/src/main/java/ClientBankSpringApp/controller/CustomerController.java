package ClientBankSpringApp.controller;

import ClientBankSpringApp.dto.CustomerRequest;
import ClientBankSpringApp.dto.CustomerResponse;
import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.enumerations.Currency;
import ClientBankSpringApp.exception.CustomerNotFoundException;
import ClientBankSpringApp.facade.AccountFacade;
import ClientBankSpringApp.facade.CustomerFacade;
import ClientBankSpringApp.facade.EmployerFacade;
import ClientBankSpringApp.service.AccountService;
import ClientBankSpringApp.service.CustomerService;
import ClientBankSpringApp.service.EmployerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("customers")
public class CustomerController {

    private final CustomerFacade customerFacade;
    private final AccountFacade accountFacade;

    //    1.Получить информацию про отдельного пользователя включая его счета
    @GetMapping("{id}")
    public CustomerResponse getOneCustomer(@RequestParam("id") long id) {
        log.info("get customer with id %d", id);
        return customerFacade.getOneCustomer(id);
    }

    //    2. Получить информацию про всех пользователей
    @GetMapping("")
    public List<CustomerResponse> getAllCustomers(
            @RequestParam(value = "page", required = false, defaultValue = "0") int page,
            @RequestParam(value = "limit", required = false, defaultValue = "10") int limit) {
        log.info("get all customers");
        return customerFacade.getAllCustomers(page, limit);
    }

    //    3. Создать пользователя
    @PostMapping("create")
    @ResponseStatus(HttpStatus.CREATED)
    public Customer createCustomer(@Validated @RequestBody CustomerRequest customerRequest) {
        log.info("create customer");
        return customerFacade.createCustomer(customerRequest);
    }

    //    4. Изменить данные пользователя
    @PutMapping("update")
    public Customer updateCustomer(@RequestParam("id") long id, @RequestBody CustomerRequest customerRequest) {
        log.info("update customer");
        return customerFacade.updateCustomer(id, customerRequest);
    }

    //    5. Удалить пользователя
    @DeleteMapping("delete")
    public void deleteCustomer(@RequestParam("id") long id) {
        log.info("delete customer with id %d", id);
        customerFacade.deleteCustomer(id);
    }

    //    6. Создать счет для конкретного пользователя
    @PostMapping("accounts/create")
    public void createAccountToCustomer(@RequestParam("id") Long id, @RequestParam("currency") String currency) {
        log.info("create account of %s for customer with id %d", currency, id);
        customerFacade.createAccount(id, currency);
    }

    //    7. Удалить счет у пользователя
    @DeleteMapping("accounts/delete")
    public void deleteAccount(@RequestParam("id") Long id) {
        log.info("delete account with id %d", id);
        accountFacade.deleteAccount(id);
    }

    //    8. Установить сотрудника
    @PostMapping({"setEmployer"})
    public void setEmployer(@RequestParam("customerID") long customerID, @RequestParam("employerId") long employerId) {
        log.info("set customer with id %d to employer with id %d", customerID, employerId);
        customerFacade.setEmployer(customerID, employerId);
    }

}
