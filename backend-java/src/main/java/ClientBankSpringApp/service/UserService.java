package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.DbUser;
import ClientBankSpringApp.exception.NoDataFoundException;
import ClientBankSpringApp.exception.UserAlreadyExistException;
import ClientBankSpringApp.repositories.DbUserRepo;
import ClientBankSpringApp.security.JwtTokenProvider;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserService {
    private final AuthenticationManager authenticationManager;
    private final DbUserRepo userRepo;
    private final JwtTokenProvider jwtTokenProvider;
    private final PasswordEncoder passwordEncoder;

    public UserService(AuthenticationManager authenticationManager, DbUserRepo userRepo, JwtTokenProvider jwtTokenProvider, PasswordEncoder passwordEncoder) {
        this.authenticationManager = authenticationManager;
        this.userRepo = userRepo;
        this.jwtTokenProvider = jwtTokenProvider;
        this.passwordEncoder = passwordEncoder;
    }

    public Map<Object, Object> createToken(DbUser user) {
        String token = jwtTokenProvider.createToken(user.getEmail(), "USER", user.getId());

        Map<Object, Object> tokens = new HashMap<>();
        tokens.put("userId", user.getId());
        tokens.put("token", token);
        return tokens;
    }

    public Map<Object, Object> authenticate(String email, String password) {
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        DbUser user = userRepo.findByEmail(email).orElseThrow(() -> new NoDataFoundException("Not found"));
        return createToken(user);
    }

    public Map<Object, Object> register(DbUser user) {
        boolean isUserExist = userRepo.findByEmail(user.getEmail()).isPresent();

        if (isUserExist) {
            throw new UserAlreadyExistException("User already exist");
        } else {
            String password = user.getPassword();
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            DbUser newUser = userRepo.save(user);
            return authenticate(newUser.getEmail(), password);
        }
    }

    public DbUser getProfile(String email){
        return userRepo.findByEmail(email).orElseThrow(()-> new NoDataFoundException("No found"));
    }

    public DbUser create(DbUser user) {
        return new DbUser();
    }

    public void deleteById(long id) {
        userRepo.deleteById(id);
    }
}
