package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.Customer;
import ClientBankSpringApp.exception.CustomerNotFoundException;
import ClientBankSpringApp.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CustomerService{
    private final CustomerRepository customerRepository;

    public void deleteById(long id) {
        if (this.customerRepository.getOne(id).getId()==id) this.customerRepository.deleteById(id);
        else throw new CustomerNotFoundException(id);
    }

    public Customer create(Customer customer) {
        return this.customerRepository.save(customer);
    }

    public Customer getCustomer(long id) {
        return customerRepository.getOne(id);
    }

    public Page<Customer> getAllCustomers(int page, int limit) {
        return customerRepository.findAll(PageRequest.of(page, limit));
    }

    public Customer update(long id) {
        if (this.customerRepository.getOne(id).getId()==id) {
            Customer customer = this.customerRepository.getOne(id);
            return this.customerRepository.save(customer);
        }
        else throw new CustomerNotFoundException(id);
    }
}
