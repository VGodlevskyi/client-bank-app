package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.Account;
import ClientBankSpringApp.repositories.AccountRepository;
import ClientBankSpringApp.repositories.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountRepository accountRepository;

    public Optional<Account> get(Long id) {
        return accountRepository.findById(id);
    }

    public List<Account> getAll() {
        return this.accountRepository.findAll();
    }

    public Account saveAccount(Account account) {
        return accountRepository.save(account);
    }

    public Optional<Account> getAccount(String number) {
        return getAll().stream().filter(a -> a.getNumber().equals(number)).findAny();
    }

    public void deleteById(Long id) {accountRepository.deleteById(id);
    }
}
