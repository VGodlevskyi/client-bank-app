package ClientBankSpringApp.service;

import ClientBankSpringApp.entity.Employer;
import ClientBankSpringApp.repositories.EmployerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class EmployerService {
    private final EmployerRepository employerRepository;

    public List<Employer> getAll() {
        return employerRepository.findAll();
    }

    public void delete(Long id) {
        employerRepository.deleteById(id);
    }

    public Employer getById(Long id) {
        return employerRepository.getOne(id);
    }

    public Employer save(Employer employer) {
        return employerRepository.save(employer);
    }
}
