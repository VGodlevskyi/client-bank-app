package ClientBankSpringApp.dao;

import ClientBankSpringApp.entity.Employer;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class CollectionEmployerDao implements Dao<Employer> {
    private final List<Employer> employers;
    private long idCounter;

    public CollectionEmployerDao(List<Employer> employers) {
        this.employers = employers;
    }

    @Override
    public Employer save(Employer employer) {
        if(findEmployer(employer)) {
            employer.setId(idCounter++);
        } else {
            this.deleteById(employer.getId());
        }
        this.employers.add(employer);
        return employer;
    }

    @Override
    public boolean delete(Employer obj) {
        return this.employers.remove(obj);
    }

    @Override
    public void deleteAll(List<Employer> entities) {
        this.employers.removeAll(entities);
    }

    public boolean findEmployer(Employer employer) {
        return employer.getId() == null;
    }

    @Override
    public void saveAll(List<Employer> entities) {
        this.employers.forEach(item -> {
            if (findEmployer(item)) {
                item.setId(idCounter++);
            } else {
                this.deleteById(item.getId());
            }
            this.employers.add(item);
        });
    }

    @Override
    public List<Employer> findAll() {
        return this.employers;
    }

    @Override
    public boolean deleteById(long id) {
        return employers.removeIf(el -> el.getId().equals(id));
    }

    @Override
    public Employer getOne(long id) {
        return this.employers.stream()
                .filter(el -> el.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
