package ClientBankSpringApp.dao;

import ClientBankSpringApp.entity.Account;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.List;

@Component
public class CollectionAccountDao implements Dao<Account> {
    private final List<Account> accounts;
    private long idCounter;

    public CollectionAccountDao() {
        this.accounts = new ArrayList<>();
        this.idCounter = 1;
    }

    public Account findByNumber(String number) {
        return this.accounts.stream()
                .filter(a -> a.getNumber().equals(number))
                .findFirst().orElseThrow(IllegalAccessError::new);
    }

    public boolean findAccount(Account account) {
        return account.getId() != null;
    }

    @Override
    public Account save(Account account) {
        if (!findAccount(account)) {
            account.setId(idCounter++);
        } else {
            this.deleteById(account.getId());
        }
        this.accounts.add(account);
        return account;
    }

    @Override
    public boolean delete(Account account) {
        return this.accounts.remove(account);
    }

    @Override
    public void deleteAll(List<Account> entities) {
        this.accounts.removeAll(entities);
    }

    @Override
    public void saveAll(List<Account> entities) {
        entities.forEach(item->{
            if (!findAccount(item)) {
                item.setId(idCounter++);
            } else {
                this.deleteById(item.getId());
            }
            this.accounts.add(item);
        });
    }

    @Override
    public List<Account> findAll() {
        return this.accounts;
    }

    @Override
    public boolean deleteById(long id) {
        return this.accounts.removeIf(a -> a.getId().equals(id));
    }

    @Override
    public Account getOne(long id) {
        return this.accounts.stream()
                .filter(x->x.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
