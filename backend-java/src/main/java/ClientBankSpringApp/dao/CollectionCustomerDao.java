package ClientBankSpringApp.dao;

import ClientBankSpringApp.entity.Customer;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CollectionCustomerDao implements Dao<Customer> {
    private final List<Customer> customers;
    private long idCounter;

    public CollectionCustomerDao (){
        this.customers = new ArrayList<>();
        this.idCounter = 1;
    }

    public boolean findCustomer(Customer customer) {
        return customer.getId() != null;
    }

    @Override
    public Customer save(Customer customer) {
        if(!findCustomer(customer)) {
            customer.setId(idCounter++);
        } else {
            this.deleteById(customer.getId());
        }
        this.customers.add(customer);
        return customer;
    }
    @Override
    public boolean delete(Customer customer) {
        return this.customers.remove(customer);
    }
    @Override
    public void deleteAll(List<Customer> customers) {
        this.customers.removeAll(customers);
    }
    @Override
    public void saveAll(List<Customer> customers) {
        this.customers.forEach(item->{
            if(!findCustomer(item)){
                item.setId(idCounter++);
            } else {
                this.deleteById(item.getId());
            }
            this.customers.add(item);
        });
    }
    @Override
    public List<Customer> findAll() {
        return this.customers;
    }

    @Override
    public boolean deleteById(long id) {
        return customers.removeIf(el-> el.getId().equals(id));
    }

    public Customer findById(long id) {
        return customers.stream().filter(el-> el.getId().equals(id)).findFirst().orElse(null);
    }

    @Override
    public Customer getOne(long id) {
        return this.customers.stream()
                .filter(el -> el.getId().equals(id))
                .findFirst()
                .orElse(null);
    }
}
