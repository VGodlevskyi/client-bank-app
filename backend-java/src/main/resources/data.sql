insert into customers (age, email, name) values (45, 'vgodlevskyi@gmail.com', 'Vitalii');
insert into customers (age, email, name) values (33, 'natali@gmail.com', 'Natali');

insert into employers (name, address ) values ('Pavel', 'Kiev');
insert into employers (name, address ) values ('Olga', 'Paris');

insert into accounts (number, currency, balance) values (1245156, 1, 100);
insert into accounts (number, currency, balance) values (11000, 2, 200);

insert into accounts_to_customer (customer_id, account_id) values (1, 1);
insert into accounts_to_customer (customer_id, account_id) values (2, 2);

insert into employers_to_customers (employer_id, customer_id) values (1, 1);
insert into employers_to_customers (employer_id, customer_id) values (1, 2);
insert into employers_to_customers (employer_id, customer_id) values (2, 1);
insert into employers_to_customers (employer_id, customer_id) values (2, 2);