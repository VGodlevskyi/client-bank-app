import React from 'react';
import {Route, Switch} from "react-router-dom";
import Dashboard from "../pages/Dashboard";
import PageCustomers from "../pages/PageCustomers";
import PageLogin from "../pages/PageLogin";
import PageEmployers from "../pages/PageEmployers";
import Page404 from "../pages/Page404";


const AppRoutes = () => {
    return (
        <>
            <Switch>
                <Route exact path="/login" render={() => <PageLogin />}/>
                <Route exact path="/" render={() => <Dashboard />}/>
                <Route exact path="/customers" render={() => <PageCustomers />}/>
                <Route exact path="/employers" render={() => <PageEmployers />}/>
                <Route path="*" render={() => <Page404/>}/>
            </Switch>
        </>
    );
};

export default AppRoutes;