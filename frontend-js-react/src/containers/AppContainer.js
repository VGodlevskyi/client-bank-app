import React, {lazy, Suspense, useMemo} from 'react';
import PrivateRoute from "../components/PrivateRoute/PrivateRoute";
import {Switch} from "react-router-dom";

export const routes = [
    {
        isPublic: true,
        exact: true,
        path: '/login',
        component: lazy(() => import("../pages/PageLogin"))
    },
    {
        isPublic: false,
        exact: true,
        path: '/customers',
        component: lazy(() => import("../pages/PageCustomers"))
    },
    {
        isPublic: false,
        exact: true,
        path: '/employers',
        component: lazy(() => import("../pages/PageEmployers"))
    },
    {
        path: '/',
        notFound: true,
        component: lazy(() => import("../pages/Page404"))
    }
]

const AppContainer = () => {
    const routeComponents = useMemo(
        () => routes.map(({isPublic, ...route}) => (
            <PrivateRoute key={route.path} isPublic={isPublic} {...route}/>
        ))
    , [])

    return (
        <Suspense fallback={"Loading..."}>
            <Switch>
                {routeComponents}
            </Switch>
        </Suspense>
    );
};

export default AppContainer;