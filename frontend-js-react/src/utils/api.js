import axios from "axios";
import {setToken} from "./tokens";

const api = axios.create();

api.interceptors.response.use(
    res => res.data,
    async function (error) {
        if (error.response.status === 401 || error.response.status === 403) {
            setToken();
            // eslint-disable-next-line no-restricted-globals
            history.push("/login");
        }
        return Promise.reject(error);
    }
);


export default api;