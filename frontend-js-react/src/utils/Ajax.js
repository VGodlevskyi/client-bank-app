import axios from "axios";
import {getToken} from "./tokens";

class Ajax {

    static async get(url) {
        const {data} = await axios.get(`${url}`)
        console.log('get all data-->', data);
        return data
    }

    static async post(url, object) {
        const {data} = await axios.post(url, object,
            {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': getToken()
                }
            }
        )
        console.log(getToken());
        console.log('post new data-->', data);
        return data;
    }


    static async put(url,  updatedObject) {
        const {data} = await axios.put(url, updatedObject, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getToken()
            }
        })
        console.log('update old data-->', data);
        return data;
    }

    static async delete(url) {
        const {data} = await axios.delete(url, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': getToken()
            }
        })
        console.log("'Bearer' + getToken()");
        console.log('delete data-->', data);
        return data;
    }
}

export default Ajax;