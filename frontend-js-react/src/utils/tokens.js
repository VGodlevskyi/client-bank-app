import api from './api';

export const getToken = () => {
    return localStorage.getItem("token");
}

export const setToken = (token) => {
    if (token) {
        api.defaults.headers.common.Authorization = `${token}`;
        localStorage.setItem("token", token);
    } else {
        delete api.defaults.headers.common.Authorization;
        localStorage.removeItem("token");
    }
}