import {authConstants} from "./const";
import {getToken} from "../../utils/tokens";

const token = getToken();

const initialState = {
    user: {},
    isAuthenticated: Boolean(token)
}

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case authConstants.LOGIN_SUCCESS:
            return {
                ...state,
            };
        case authConstants.LOGIN_FAILURE:
            return {
                ...state,
                user: {},
                isAuthenticated: false
            };
        case authConstants.REGISTER_SUCCESS:
            return {
                ...state,
            };
        case authConstants.GET_PROFILE_SUCCESS:
            return {
                ...state,
                user: action.payload.user,
                isAuthenticated: true
            };
        case authConstants.GET_PROFILE_FAILURE:
            return {
                ...state,
                user: {},
                isAuthenticated: false
            };
        case authConstants.LOGOUT:
            return {
                user: {},
                isAuthenticated: false
            };
        default:
            return state;
    }
}