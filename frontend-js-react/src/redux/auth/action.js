import {setToken} from "../../utils/tokens";
import {authConstants} from "./const";
import api from "../../utils/api";

const getProfile = () => (dispatch) => {
    dispatch({ type: authConstants.GET_PROFILE_REQUEST });
    api({
        method: "GET",
        url: "/api/auth/profile",
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => {
        dispatch({ type: authConstants.GET_PROFILE_SUCCESS, payload: {user: res} });
    }).catch(err => {
        setToken();
        dispatch({ type: authConstants.GET_PROFILE_FAILURE, payload: {} });
    })
}

const login = (data) => (dispatch) => {
    dispatch({ type: authConstants.LOGIN_REQUEST });
    api({
        method: "POST",
        url: "/api/auth/login",
        data,
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => {
        setToken(res.token);
        dispatch({ type: authConstants.LOGIN_SUCCESS, payload: {} });
        dispatch(getProfile());
    }).catch(err => {
        dispatch({ type: authConstants.LOGIN_FAILURE, payload: {} });
    })
}

const register = (data) => (dispatch) => {
    dispatch({ type: authConstants.REGISTER_REQUEST });
    api({
        method: "POST",
        url: "/api/auth/register",
        data,
        headers: {
            "Content-Type": "application/json"
        }
    }).then(res => {
        setToken(res.token);
        dispatch({ type: authConstants.REGISTER_SUCCESS, payload: {} });
        dispatch(getProfile());
    }).catch(err => {
        console.log(err)
        dispatch({ type: authConstants.REGISTER_FAILURE, payload: {} });
    })
}



const logout = () => (dispatch) => {
    setToken();
    dispatch({ type: authConstants.LOGOUT });
}

export const authActions = {
    getProfile,
    login,
    register,
    logout
}
