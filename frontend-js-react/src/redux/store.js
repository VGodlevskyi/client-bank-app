import {createStore, combineReducers, applyMiddleware} from 'redux';
import {composeWithDevTools} from "redux-devtools-extension";
import thunk from 'redux-thunk';
import {authReducer} from "./auth/reducer";
import {getToken, setToken} from "../utils/tokens";
import {authActions} from "./auth/action";


const reducer = combineReducers({
    auth: authReducer
});

export default () => {
    const token = getToken();
    const store = createStore(
        reducer,
        composeWithDevTools(applyMiddleware(thunk))
    );

    if (token) {
        setToken(token);
        store.dispatch(authActions.getProfile());
    }

    return store;
}