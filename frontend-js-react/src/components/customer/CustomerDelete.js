import Ajax from "../../utils/Ajax";
import React, {useState} from "react";
import {TextField} from "@material-ui/core";

const CustomerDelete = () => {

    const [id, setId] = useState(1)

    const handleChange = (event) => {
        setId( event.target.value);
    }

    const submitForm = (data) => {
        Ajax.delete(`/customer/delete?id=${id}`)

    }

    return (
        <div>
            <p>DELETE CUSTOMER</p>
            <form onSubmit={submitForm}>
                <TextField id="id" name="id" label="id" onChange={handleChange} variant="outlined"/>
                <p></p>
                <input style={{margin: '0 0 0 20px'}} type="submit" value="DELETE"/>
            </form>
        </div>
    );

}
export default CustomerDelete;