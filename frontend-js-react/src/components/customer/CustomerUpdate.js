import Ajax from "../../utils/Ajax";
import React, {useState} from "react";
import {TextField} from "@material-ui/core";

const CustomerUpdate = ({data}) => {

    const [user, setUser] = useState(data || {
        id: 1, age: '', email: '', name: 'name', phone: '', password: ''
    })

    const handleChange = (event) => {
        setUser({...user, [event.target.name]: event.target.value});
    }

    const submitForm = (data) => {
        // data.preventDefault();
        Ajax.put(`/customers/update?id=${user.id}`, user)
    }

    return (
        <div>
            <p>UPDATE CUSTOMER</p>

            <form  onSubmit={submitForm}>
                <TextField id="id" name="id" label="id" onChange={handleChange} variant="outlined"/>
                <TextField id="name" name="name" label="name" onChange={handleChange} variant="outlined"/>
                <TextField id="email" name="email" label="email" onChange={handleChange} variant="outlined"/>
                <TextField id="age" name="age" label="age" onChange={handleChange} variant="outlined"/>                 {/*<label htmlFor="phone"> phone </label>*/}
                <TextField id="phone" name="phone" label="phone" onChange={handleChange} variant="outlined"/>
                <TextField id="password" name="password" label="password" onChange={handleChange} variant="outlined"/>
                <p></p>
                <input style={{margin: '0 0 0 10px'}} type="submit" value="UPDATE"/>
            </form>
        </div>
    );

}
export default CustomerUpdate;