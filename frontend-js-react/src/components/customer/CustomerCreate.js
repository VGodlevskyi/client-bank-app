import Ajax from "../../utils/Ajax";
import React, {useState} from "react";
import {TextField} from "@material-ui/core";
import InputMask from 'react-input-mask';

const CustomerCreate = () => {

    const [user, setUser] = useState({
        age: '', email: '', name: 'name'
    })

    const handleChange = (event) => {
        setUser({...user, [event.target.name]: event.target.value});
    }

    const submitForm = (data) => {
        Ajax.post("/customers/create", user)
    }

    return (
        <div>
            <p>CREATE CUSTOMER</p>
            <form  onSubmit={submitForm}>
                <TextField id="name" name="name" label="name" onChange={handleChange} variant="outlined"/>
                <TextField id="email" name="email" label="email" onChange={handleChange} variant="outlined"/>
                <TextField id="age" name="age" label="age" onChange={handleChange} variant="outlined"/>                 {/*<label htmlFor="phone"> phone </label>*/}

                <InputMask
                    mask="38(099)999-99-99"
                    onChange={handleChange}
                    disabled={false}
                    maskChar=" "
                >
                    {() => <TextField id="phone" name="phone" label="phone" variant="outlined"/>}
                </InputMask>

                <TextField id="password" name="password" label="password" onChange={handleChange} variant="outlined"/>
                <p></p>
                <input style={{margin: '0 0 0 10px'}} type="submit" value="CREATE"/>
            </form>
        </div>
    );

}
export default CustomerCreate;