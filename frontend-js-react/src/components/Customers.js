import React from 'react';
import Link from '@material-ui/core/Link';
import {makeStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Title from '../pages/Title';
import Typography from "@material-ui/core/Typography";
import CustomerCreate from "./customer/CustomerCreate";
import CustomerDelete from "./customer/CustomerDelete";
import CustomerUpdate from "./customer/CustomerUpdate";
import AccountCreate from "./account/AccountCreate";
import CustomerRow from "./CustomerRow";


function preventDefault(event) {
    event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        background: '#fff',
        padding: '15px 10px'
    }
}));

export default function Customers({allCustomers}) {
    const classes = useStyles();

    return (
        <React.Fragment>
            <Title>Customers Info</Title>
            {
                allCustomers.map(x => (
                    <div key={x.email.toString()} style={{marginBottom: '20px'}}>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Id: {x.id}
                        </Typography>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Name: {x.name}
                        </Typography>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Email: {x.email}
                        </Typography>
                        <Typography style={{marginRight: '10px', display: 'block'}} is = "custom" component="subtitle1"
                                    variant="subtitle1" color="primary" gutterBottom>
                            Accounts
                        </Typography>
                        <Table size="small">
                            <TableHead>
                                <TableRow>
                                    <TableCell>number</TableCell>
                                    <TableCell>currency</TableCell>
                                    <TableCell>balance</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {x.accounts.map((row) => (
                                    <CustomerRow key={row.id.toString()} row={row}/>
                                ))}

                            </TableBody>

                        </Table>
                        <AccountCreate/>
                    </div>
                ))
            }
            <CustomerUpdate/>
            <CustomerCreate/>
            <CustomerDelete/>

            <div className={classes.seeMore}>
                <Link color="primary" href="#" onClick={preventDefault}>
                    See more customers
                </Link>
            </div>
        </React.Fragment>
    );
}
