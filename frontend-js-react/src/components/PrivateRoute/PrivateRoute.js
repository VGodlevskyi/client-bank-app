import React from 'react';
import {Route, Redirect} from "react-router-dom";
import {useSelector} from "react-redux";

const PrivateRoute = ({ isPublic, ...route }) => {
    const { isAuthenticated } = useSelector(state => state.auth);


    if (isPublic || isAuthenticated) return <Route {...route}/>

    return <Redirect to="/login"/>
};

export default PrivateRoute;