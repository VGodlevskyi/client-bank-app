import React from 'react';
import Link from '@material-ui/core/Link';
import {makeStyles} from '@material-ui/core/styles';
import Title from '../pages/Title';
import Typography from "@material-ui/core/Typography";
import EmployerCreate from "./employer/EmployerCreate";


function preventDefault(event) {
    event.preventDefault();
}

const useStyles = makeStyles((theme) => ({
    seeMore: {
        marginTop: theme.spacing(3),
    },
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        background: '#fff',
        padding: '15px 10px'

    }
}));

export default function Employers({allEmployers}) {
    const classes = useStyles();


    return (
        <React.Fragment>
            <Title>Employers Info</Title>
            {
                allEmployers.map(x => (
                    <div  key={x.name.toString()} style={{marginBottom: '20px'}}>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Id: {x.id}
                        </Typography>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Name: {x.name}
                        </Typography>
                        <Typography style={{marginRight: '10px'}} is = "custom" component="subtitle1" variant="subtitle1"
                                    color="primary" gutterBottom>
                            Adress: {x.address}
                        </Typography>
                            
                    </div>
                ))
            }
            <EmployerCreate/>

            <div className={classes.seeMore}>
                <Link color="primary" href="#" onClick={preventDefault}>
                    See more employers
                </Link>
            </div>
        </React.Fragment>
    );
}
