import Ajax from "../../utils/Ajax";
import React, {useState} from "react";
import {TextField} from "@material-ui/core";

const EmployerCreate = () => {

    const [employer, setEmployer] = useState({
        name: '', address: ''
    })
    const [idDelete, setIdDelete] = useState(0);
    const [empCus, setEmpCus] = useState({employerId: 0, customerId: 0});

    const assignEmployer = (event) => {
        setEmpCus({...empCus, [event.target.name]: event.target.value});
    }

    const handleChange = (event) => {
        setEmployer({...employer, [event.target.name]: event.target.value});
    }

    const handleChangeDel = (event) => {
        setIdDelete({...employer, [event.target.name]: event.target.value});
    }

    const createEmployer = () => {
        Ajax.post(`employers/create`, employer)
    }

    const assignEmployerToCustomer = () => {
        Ajax.post(`customers/setEmployer?customerId=${empCus.customerId}&employerId=${empCus.employerId}`,)
    }

    const deleteEmployer = () => {
        Ajax.delete(`employers/delete?id=${idDelete}`)
    }

    return (
        <div>
            <div style={{display: "block"}}>
                <p>CREATE EMPLOYER</p>
                <form onSubmit={createEmployer}>
                    <TextField id="employerName" name="name" label="employer name" onChange={handleChange}
                               variant="outlined"/>
                    <TextField id="employerAdd" name="address" label="employer address" onChange={handleChange}
                               variant="outlined"/>
                    <p></p>
                    <input style={{margin: '0 0 0 10px'}} type="submit" value="CREATE"/>
                </form>
            </div>
            <div style={{display: "block", marginLeft: '20px'}}>
                <p>DELETE EMPLOYER</p>
                <form onSubmit={deleteEmployer}>
                    <TextField id="employerId" name="employerId" label="employer ID" onChange={handleChangeDel}
                               variant="outlined"/>
                    <p></p>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="DELETE"/>
                </form>
            </div>
            <div style={{display: "block", marginLeft: '20px'}}>
                <p>ASSIGN EMPLOYER</p>
                <form onSubmit={assignEmployerToCustomer}>
                    <TextField id="customerId" name="customerId" label="customer ID" onChange={assignEmployer}
                               variant="outlined"/>
                    <TextField id="employerId" name="employerId" label="employer ID" onChange={assignEmployer}
                               variant="outlined"/>
                    <p></p>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="ASSIGN"/>
                </form>
            </div>
        </div>
    );

}
export default EmployerCreate;