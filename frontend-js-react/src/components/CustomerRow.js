import React, {useState} from 'react';
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import {Edit} from "@material-ui/icons";
import {Modal} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import AccountReplenish from "./account/AccountReplenish";
import AccountWithdraw from "./account/AccountWithdraw";
import AccountTransfer from "./account/AccountTransfer";
import AccountCreate from "./account/AccountCreate";
import AccountDelete from "./account/AccountDelete";


const useStyles = makeStyles((theme) => ({
    modal: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        transform: 'translate(-50%, -50%)',
        background: '#fff',
        padding: '15px 10px'

    }
}));

const CustomerRow = ({row}) => {

    const classes = useStyles();

    const [isOpenEditModalR, setOpenEditModalR] = useState(false);
    const [isOpenEditModalW, setOpenEditModalW] = useState(false);
    const [isOpenEditModalT, setOpenEditModalT] = useState(false);
    const [isOpenEditModalC, setOpenEditModalC] = useState(false);
    const [isOpenEditModalD, setOpenEditModalD] = useState(false);

    const handleOpenEditModalR = () => setOpenEditModalR(prev => !prev);
    const handleOpenEditModalW = () => setOpenEditModalW(prev => !prev);
    const handleOpenEditModalT = () => setOpenEditModalT(prev => !prev);
    const handleOpenEditModalC = () => setOpenEditModalC(prev => !prev);
    const handleOpenEditModalD = () => setOpenEditModalD(prev => !prev);
    return (
        <>

            <TableRow key={row.number.toString()}>
                <TableCell>{row.number}</TableCell>
                <TableCell>{row.currency}</TableCell>
                <TableCell>{row.balance}</TableCell>

                <TableCell>
                    <IconButton onClick={handleOpenEditModalR}>
                        <p>Replenish</p>
                        <Edit/>
                    </IconButton>
                </TableCell>
                <TableCell>
                    <IconButton onClick={handleOpenEditModalW}>
                        <p>Withdraw</p>
                        <Edit/>
                    </IconButton>
                </TableCell>
                <TableCell>
                    <IconButton onClick={handleOpenEditModalT}>
                        <p>Transfer</p>
                        <Edit/>
                    </IconButton>
                </TableCell>
                <TableCell>
                    <IconButton onClick={handleOpenEditModalD}>
                        <p>DeleteAccount</p>
                        <Edit/>
                    </IconButton>
                </TableCell>
            </TableRow>



            <Modal open={isOpenEditModalR} onClose={handleOpenEditModalR}>
                <div className={classes.modal}>
                    <AccountReplenish data={{accountNumber: row.number, balance: row.balance}}/>
                </div>
            </Modal>
            <Modal open={isOpenEditModalW} onClose={handleOpenEditModalW}>
                <div className={classes.modal}>
                    <AccountWithdraw data={{accountNumber: row.number, balance: row.balance}}/>
                </div>
            </Modal>
            <Modal open={isOpenEditModalT} onClose={handleOpenEditModalT}>
                <div className={classes.modal}>
                    <AccountTransfer data={{from: row.from, to: row.to, sum: row.sum}}/>
                </div>
            </Modal>
            <Modal open={isOpenEditModalC} onClose={handleOpenEditModalC}>
                <div className={classes.modal}>
                    <AccountCreate data={{accountNumber: row.number, currency: row.currency}}/>
                </div>
            </Modal>
            <Modal open={isOpenEditModalD} onClose={handleOpenEditModalD}>
                <div className={classes.modal}>
                    <AccountDelete handleModal={handleOpenEditModalD} data={{id: row.id}}/>
                </div>
            </Modal>


        </>

    );
};

export default CustomerRow;