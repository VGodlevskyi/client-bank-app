import Ajax from "../../utils/Ajax";
import {useState} from "react";

const AccountCreate = ({data}) => {
    const [id, setId] = useState(0);
    const [currency, setCurrency] = useState("USD");

    const handleChangeCurrency = (event) => {
        setCurrency(event.target.value);
    }
    const handleChangeId = (event) => {
        setId(event.target.value);
        console.log(event.target.value)
    }

    const createAccount = (e) => {
        // e.preventDefault();
        Ajax.post(`customers/accounts/create?id=${id}&currency=${currency}`)
    }

    return (
        <>
            <div style={{display: "inline-block"}}>
                <p>CREATE ACCOUNT</p>
                <form onSubmit={createAccount}>
                    <label htmlFor="userID"> customer ID </label>
                    <input id="userID" name="userID" type="text" onChange={handleChangeId}/>
                    <select onChange={handleChangeCurrency}>
                        <option name="currency" value="USD">USD</option>
                        <option name="currency" value="EUR">EUR</option>
                        <option name="currency" value="UAH">UAH</option>
                        <option name="currency" value="CHF">CHF</option>
                        <option name="currency" value="GBP">GBP</option>
                    </select>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="CREATE"/>
                </form>
            </div>
        </>
    );

}




//     const [account, setAccount] = useState(data || {
//         id: '', currency: 'EUR'
//     })
//
//     const handleChange = (event) => {
//         setAccount({...account, [event.target.name]: event.target.value});
//     }
//
//     const createAccount = () => {
//         Ajax.post(`customer/accounts/create?id=${account.id}&currency=${account.currency}`)
//     }
//
//     return (
//         <div>
//             <div style={{display: "inline-block"}}>
//                 <p>CREATE ACCOUNT</p>
//                 <form onSubmit={createAccount}>
//                     <label htmlFor="userID"> customer ID </label>
//                     <input id="userID" name="userID" type="text" onChange={handleChange} />
//                     <select onChange={handleChange}>
//                         <option name="currency" value="USD">USD</option>
//                         <option name="currency" value="EUR">EUR</option>
//                         <option name="currency" value="UAH">UAH</option>
//                         <option name="currency" value="CHF">CHF</option>
//                         <option name="currency" value="GBP">GBP</option>
//                     </select>
//                     <input style={{margin: '0 0 0 20px'}} type="submit" value="CREATE"/>
//                 </form>
//             </div>
//         </div>
//     );
//
// }
export default AccountCreate;