import Ajax from "../../utils/Ajax";
import {useState} from "react";

const AccountDelete = ({handleModal, data}) => {
    const [account, setAccount] = useState(data || {
        accountNumber: ''
    })

    const handleChange = (event) => {
        setAccount({...account, [event.target.name]: event.target.value});
    }

    const deleteAccount = (e) => {
        // e.preventDefault();
        Ajax.delete(`customers/accounts/delete?id=${account.id}`)
        handleModal();
    }

    return (
        <div>
            <div style={{display: "inline-block", marginLeft: '20px'}}>
                <p>DELETE ACCOUNT</p>
                <form onSubmit={deleteAccount}>
                    <label htmlFor="accountNumber"> account Number </label>
                    <input id="accountNumber" name="accountNumber" value={account.accountNumber} type="text" onChange={handleChange}/>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="DELETE"/>
                </form>
            </div>
        </div>
    );

}
export default AccountDelete;