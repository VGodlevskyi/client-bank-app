import Ajax from "../../utils/Ajax";
import {useState} from "react";

const AccountReplenish = ({data}) => {

    const [account, setAccount] = useState(data || {
        accountNumber: '', balance: 0
    })

    const handleChange = (event) => {
        setAccount({...account, [event.target.name]: event.target.value});
    }

    const submitForm = () => {
        // data.preventDefault();
        Ajax.put(`/account/replenish?accountNumber=${account.accountNumber}`, account)
    }

    return (
        <div>
            <div style={{display: "inline-block"}}>
                <p>REPLENISH MONEY</p>
                <form onSubmit={submitForm}>
                    <label htmlFor="accountNumber"> account number </label>
                    <input id="accountNumber" name="accountNumber" type="text" value={account.accountNumber} onChange={handleChange}/>
                    <label htmlFor="balance"> amount </label>
                    <input id="balance" name="balance" type="text" value={account.balance} onChange={handleChange}/>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="PUT"/>
                </form>
            </div>
        </div>
    );

}
export default AccountReplenish;