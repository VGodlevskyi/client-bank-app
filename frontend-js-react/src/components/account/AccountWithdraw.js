import Ajax from "../../utils/Ajax";
import {useState} from "react";

const AccountWithdraw = ({data}) => {

    const [account, setAccount] = useState(data || {
        accountNumber: '', balance: 0
    })

    const handleChange = (event) => {
        setAccount({...account, [event.target.name]: event.target.value});
    }

    const withdrawMoney = () => {
        Ajax.put(`/account/withdraw?accountNumber=${account.accountNumber}`, account)
    }

    return (
        <div>
            <div style={{display: "inline-block", marginLeft: '20px'}}>
                <p>WITHDRAW MONEY</p>
                <form onSubmit={withdrawMoney}>
                    <label htmlFor="accountNumber"> account number </label>
                    <input id="accountNumber" name="accountNumber" value={account.accountNumber} type="text" onChange={handleChange}/>
                    <label htmlFor="balance"> amount </label>
                    <input id="balance" name="balance" type="text" value={account.balance} onChange={handleChange}/>
                    <input style={{margin: '0 0 0 20px'}} type="submit" value="WITHDRAW"/>
                </form>
            </div>
        </div>
    );

}
export default AccountWithdraw;